#include <expressiongraph/hqp_solver.hpp>
#include <expressiongraph/hqp_messages.hpp>
#include <iostream>
#include <boost/chrono.hpp>

#define DEBUG 0

namespace KDL {
  
hqpSolver::hqpSolver(int _nWSR, double _cputime,double _regularisation_factor ):
    regularisation_factor(_regularisation_factor),nWSR(_nWSR),cputime(_cputime) {
}

int hqpSolver::setupCommon(Context::Ptr _ctx) {
    ctx           = _ctx;
    ndx_time      = ctx->getScalarNdx("time");
    // construct index of all variables: nv_robot, nv_feature and the time variables (i.e. the state) :
    all_ndx.clear();
    ctx->getScalarsOfType("robot",all_ndx);  
    nv_robot      = all_ndx.size();
    ctx->getScalarsOfType("feature",all_ndx);  
    nv_feature    = all_ndx.size() - nv_robot;
    if (DEBUG) {
    std::cout << "nv_feature: " << nv_feature << std::endl;
    std::cout << "nv_robot: " << nv_robot << std::endl;}
    all_ndx.push_back(ndx_time);
    // allocate and initialize state:
    state.resize(all_ndx.size());
    for (size_t i=0;i<all_ndx.size();++i) {
        VariableScalar* vs = ctx->getScalarStruct(all_ndx[i]);
        state[i] = vs->initial_value;
    }
    // add all expressions to the optimizer (context + output specification + monitors):
    expr_opt.prepare(all_ndx); 
    ctx->addToOptimizer(expr_opt);
    
    // find the lowest priority levels (the higher the less prioritized)
    int lowest_priority = 0;
    for (size_t indx=0;indx< ctx->cnstr_scalar.size();++indx) {
      ConstraintScalar& c = ctx->cnstr_scalar[indx];
      if (c.priority > lowest_priority)
	lowest_priority = c.priority;
    }
    
    // count and check the number of constraints for the different priority levels:      
    if (lowest_priority > 2)
      nc_priority.resize(lowest_priority+1);
    else
      nc_priority.resize(3); // there should be at least nc_priority for level 0, 1, and 2
      
    if (DEBUG){
    std::cout << "nc_priority: " << std::endl;
    for(int i=0; i < nc_priority.size(); i++)
      std::cout << nc_priority.at(i) << ' ' << std::endl;}
    
   
    for (size_t i=0;i<nc_priority.size();++i) {
        nc_priority[i]=0;
    }
    
    /* calculate the total number of soft constraints */
    nc_soft = 0; 
    for (size_t indx=0;indx< ctx->cnstr_scalar.size();++indx) {
        ConstraintScalar& c = ctx->cnstr_scalar[indx];
	
	if (c.priority >1)
	  nc_soft++;
	
        if (c.active) {
            if (c.priority >= (int)nc_priority.size()) {
                return -1;
            }
            nc_priority[ c.priority ] += 1;
        }
    }
    if (DEBUG) {
    std::cout << "Total number of soft constraints: "<< nc_soft << std::endl;}
    return 0;
}

void hqpSolver::reset() {
    H         = Eigen::MatrixXd::Identity(nv,nv)*regularisation_factor;
    A         = Eigen::MatrixXd::Zero(nc,nv);
    lb        = Eigen::VectorXd::Constant(nv,-HUGE_VALUE);
    lbA       = Eigen::VectorXd::Constant(nc,-HUGE_VALUE);
    ub        = Eigen::VectorXd::Constant(nv,HUGE_VALUE);
    ubA       = Eigen::VectorXd::Constant(nc,HUGE_VALUE);
    g         = Eigen::VectorXd::Zero(nv);  
    prev_sol_primal = Eigen::VectorXd::Zero(nv_robot+nv_feature);  
    
    h_x.clear();
    h_i.clear();
    h_p.clear();
    
    a_x.clear();
    a_i.clear();
    a_p.clear();
    
    if (initialization) {
      n         = nv;
      m         = nc;
    }
    else {
      n         = nv;
      m         = nc+nv;         
    }
          
    // Populate data
    settings = (OSQPSettings *)c_malloc(sizeof(OSQPSettings));
    data = (OSQPData *)c_malloc(sizeof(OSQPData));    
}

void hqpSolver::setupMatrices(int _nv, int _nc) {
    nv        = _nv;
    nc        = _nc;
    reset(); // resetting the matrices and vectors sizes
    
    if (initialization) 
      solution.resize(nv);
}

void hqpSolver::printMatrices(std::ostream& os) {
    os << "H:\n" << H << "\n";
    os << "lb:\n" << lb.transpose() << "\n";
    os << "ub:\n" << ub.transpose() << "\n";
    os << "A:\n" << A << "\n";
    os << "lbA:\n" << lbA.transpose() << "\n";
    os << "ubA:\n" << ubA.transpose() << std::endl; 
    os << "solution:\n" << solution.transpose() << std::endl; 
    os << "state:\n" << state.transpose() << std::endl; 
}

void hqpSolver::dense_to_csc() {
//   std::cout << "converting from dense to csc formats... " << std::endl;
  // Convert from the dense to sparse matrix representation 
  // Convert the constraint matrix
  int nrows = A.rows();
  int ncols = A.cols();  
  
  A_nnz = 0; // non-zero elements counter
  a_p.push_back(0); // at the beginning, there is 0 non-zero elements
  a_p[0] = 0;
  for (int j=0; j < ncols; ++j) {
     for (int i=0; i < nrows; ++i) {
        if (A.coeff(i,j) !=0.0) {
	   a_x.push_back(A.coeff(i,j));
	   a_i.push_back(i);
           A_nnz = A_nnz+1;	   
        }
     }
     a_p.push_back(A_nnz);
  }

  // convert the hessian matrix
  nrows = H.rows();
  ncols = H.cols();  
  
  H_nnz = 0; // non-zero elements counter
  h_p.push_back(0); // at the beginning, there is 0 non-zero elements
  
  for (int j=0; j < ncols; ++j) {
     for (int i=0; i < nrows; ++i) {
        if (H.coeff(i,j) !=0.0) {
	   h_x.push_back(H.coeff(i,j));
	   h_i.push_back(i);	   
           H_nnz = H_nnz+1;
        }
     }
     h_p.push_back(H_nnz);
  }  
}

int hqpSolver::prepareInitialization(Context::Ptr ctx) {
    int retval = setupCommon(ctx);
    if (retval!=0) return retval;
    initialization = true;
    firsttime      = true;
    priority_stage = 2;
    // set up x variable as : feature 
    optim_ndx.clear();
    ctx->getScalarsOfType("feature",optim_ndx);  
    setupMatrices( nv_feature + nc_priority[0],   nc_priority[0] );

    return 0;
}

int hqpSolver::prepareExecution(Context::Ptr ctx) {
    firsttime  = true;
    /* NOTE: In HQP, prepareExecution needs to be called at every time step because the size of the 
     constraint matrix needs to be updated due to the different priority levels */
    prty_lvl_idx.clear(); // clear the indices of soft constraints in a priority level
    int retval = setupCommon(ctx);
    if (retval!=0) return retval;
    
    if (initialization) 
       priority_stage = 2; // start by solving constraint with priority levels 0, 1 and 2

    // set up x variable as : robot | feature | slack
    optim_ndx.clear();
    ctx->getScalarsOfType("robot",optim_ndx);  
    ctx->getScalarsOfType("feature",optim_ndx);  
    
    // At the beginning of each control sample, take into account the hard constraint and the most-prioritized soft constraint
    setupMatrices( nv_robot+nv_feature+nc_priority[2],   nc_priority[0]+nc_priority[1]+nc_priority[2]  );

    // resize solution vector size
    solution.resize(nv_robot + nv_feature + nc_soft);
    
    if (firsttime)
      solution.setZero();
    initialization = false;    
    return 0; 
}

void hqpSolver::setupNextPriorityLevel() {
    /* Compute the needed matrix's dimension according to the 
     * number of priority at the given level. Hard constraints
     * (priority level=0 and 1) are always taken into account */
    int _nc = nc_priority[0]+nc_priority[1]; // number of rows constraints to solve at a given stage
    int _nv = nv_robot+nv_feature+nc_priority[priority_stage]; // number of variables to optimize at a given stage (including slack variables)
    for (int i = 2; i <= priority_stage; ++i) 
      _nc = _nc + nc_priority[i]; 
        
    setupMatrices( _nv, _nc); 
}

void hqpSolver::fillHessian() {
    // valid for both execution and initialization
    // optim_ndx contains the variable indices of the variables involved in the optimization (not including slack variables)
    for (size_t i = 0; i < optim_ndx.size(); ++i ) {
        VariableScalar* vs = ctx->getScalarStruct( optim_ndx[i] );
        H(i,i)=vs->weight->value()*regularisation_factor;
    }
    
    if (!initialization)  {
      if (DEBUG) 
	std::cout << "H: " << std::endl << H << std::endl;  }    
        
}

void hqpSolver::fillConstraintScalar(const ConstraintScalar& c) {
    if (initialization && c.priority!=0) return;
    assert( (0<=cnr)&&(cnr<nc));

    //called for updating the model
    double modelval  = c.model->value();    
    double modelder  = c.model->derivative(ndx_time); 
    double measval   = c.meas->value();
//     std::cout << "cnr: " << cnr << std::endl;
    if ((c.priority < priority_stage) && (c.priority > 1)) {
    // take into account soft constraints with higher priority levels
      if (c.target_lower==-HUGE_VALUE) {
	  lbA(cnr)        = -HUGE_VALUE;
      } else {
	  lbA(cnr)        = c.controller_lower->compute(c.target_lower-measval, -modelder) - solution[nv_robot+nv_feature+cnr_global];
      }
      if (c.target_upper==HUGE_VALUE) {
	  ubA(cnr)        = HUGE_VALUE;
      } else {
	  ubA(cnr)        = c.controller_upper->compute(c.target_upper-measval, -modelder) - solution[nv_robot+nv_feature+cnr_global];
      }
    } 
    else {
      if (c.target_lower==-HUGE_VALUE) {
	  lbA(cnr)        = -HUGE_VALUE;
      } else {
	  lbA(cnr)        = c.controller_lower->compute(c.target_lower-measval, -modelder);
      }
      if (c.target_upper==HUGE_VALUE) {
	  ubA(cnr)        = HUGE_VALUE;
      } else {
	  ubA(cnr)        = c.controller_upper->compute(c.target_upper-measval, -modelder);
      }      
    }
    
    for (size_t i=0;i<optim_ndx.size();++i) {
        A(cnr,i) = c.model->derivative(optim_ndx[i]);
    }

    if ((c.priority==priority_stage) || (initialization && (c.priority==0))) {
        // soft constraint: additional slack var:
        // assert( (0<=softcnr)&&(softcnr<nc_priority[2]));   not correct when initializing
        int j = optim_ndx.size() + softcnr;
	A(cnr,j) = 1.0;
	softcnr++; 
	H(j,j)   = regularisation_factor + c.weight->value();	
	prty_lvl_idx.push_back(cnr_global);
    }
    cnr++;
}

void hqpSolver::fillConstraintBox(const ConstraintBox& c) {
  
    for (size_t i=0;i<optim_ndx.size();++i) {
        if (optim_ndx[i]==c.variablenr) {
            lb[i] = c.target_lower;
            ub[i] = c.target_upper;
        }
    }
}

void hqpSolver::fill_constraints() {
    reset();
    softcnr  = 0;
    cnr      = 0;
    cnr_global = 0;
    prty_lvl_idx.clear();
    
    /* iterate through all of the scalar constraints according to the priority level currently being solved */
    for (size_t indx=0;indx<ctx->cnstr_scalar.size();++indx) {
        ConstraintScalar& c = ctx->cnstr_scalar[indx];
        if ((c.active) && (c.priority <= priority_stage)) {
        // If the constraint's priority is less than or equal the current priority_stage flag, then fill the matrices
          fillConstraintScalar(c);
        }
        if (c.priority >1)
	  cnr_global++;
    }    
       
    if (!initialization) 
    /* iterate through all of the box constraints */
    for (size_t indx=0;indx<ctx->cnstr_box.size();++indx) {
        ConstraintBox& c = ctx->cnstr_box[indx];
        if (c.active) {
            fillConstraintBox(c);
        }
    }        
    
    // Append nv X nv identity matrix to the constraint matrix
    if (!initialization)  {
      if (DEBUG){
      std::cout << "Appending the box constraints... " << std::endl;
      }
      A.conservativeResize(A.rows()+nv, A.cols());
      A.block(nc,0,nv, nv) =  Eigen::MatrixXd::Identity(nv, nv);
      if (DEBUG) std::cout << "A: " << std::endl << A << std::endl;


      // Append lb to lbA and ub to ubA
      lbA.conservativeResize(lbA.size()+nv);
      lbA.tail(nv)=lb;
      if (DEBUG)    std::cout << "lbA: " << std::endl << lbA << std::endl;
      
      ubA.conservativeResize(ubA.size()+nv);
      ubA.tail(nv)=ub;
      if (DEBUG)    std::cout << "ubA: " << std::endl << ubA << std::endl;
    }
}

int hqpSolver::solve() {
    int _nWSR = nWSR;
    double _cputime = cputime;
    c_int retval = 0;

    boost::chrono::high_resolution_clock::time_point start_eval = boost::chrono::high_resolution_clock::now();
    expr_opt.setInputValues(state);
    if (nv==0) return 0;

    if (initialization) {
        fill_constraints(); // fill in the constraint matrix
        fillHessian(); 
        
        boost::chrono::duration<double> sec_eval;
        sec_eval  = boost::chrono::high_resolution_clock::now() - start_eval;
        timing_eval = sec_eval.count();
        boost::chrono::high_resolution_clock::time_point start_solve = boost::chrono::high_resolution_clock::now();

        /* convert to csc form */
        dense_to_csc();

        /* convert to C++ array form */
        A_x = (double*) a_x.data();
        A_i = (c_int*) a_i.data();
        A_p = (c_int*) a_p.data();
        H_x = (double*) h_x.data();
        H_i = (c_int*) h_i.data();
        H_p = (c_int*) h_p.data();
        l   = (double*) lbA.data();
        u   = (double*) ubA.data();
        q   = (double*) g.data();

        // Solve with OSQP
        data->n = n;
        data->m = m;
        data->P = csc_matrix(data->n, data->n, H_nnz, H_x, H_i, H_p);
        data->q = q;
        data->A = csc_matrix(data->m, data->n, A_nnz, A_x, A_i, A_p);
        data->l = l;
        data->u = u;

        if (DEBUG) std::cout << "Setting up the solver's workspace " << std::endl;
        // setup workspace
        c_int retval = 0;
        if (firsttime) {
           if (settings) osqp_set_default_settings(settings);
           settings->alpha = 1;
           if (DEBUG) settings->verbose = 1;
           else       settings->verbose = 0;
           settings->max_iter = nWSR;

           retval = osqp_setup(&work, data, settings);
           firsttime = false;
        } else {
           osqp_warm_start_x(work, (double*) solution.data());
           osqp_update_lin_cost(work, q);
           osqp_update_bounds(work, l, u);
        }
         
        // solve the qp problem
        osqp_solve(work); 

        boost::chrono::duration<double> sec_solve;
        sec_solve  = boost::chrono::high_resolution_clock::now() - start_solve;
        timing_solve = sec_solve.count();
     
        solution = Eigen::Map<Eigen::VectorXd>(work->solution->x, nv);
    } else {
        boost::chrono::duration<double> sec_eval;
        sec_eval  = boost::chrono::high_resolution_clock::now() - start_eval;
        timing_eval = sec_eval.count();

        boost::chrono::high_resolution_clock::time_point start_solve;
        timing_solve = 0.0;

        // if in execution phase, then iterate through the priority levels
        if (DEBUG) 
            std::cout << "********************************************************** Solving for one time step ********************************************************** " << std::endl;            
        while ( priority_stage < nc_priority.size() ) {
            start_eval = boost::chrono::high_resolution_clock::now();
            if (DEBUG) 
                std::cout << "*******  Starting with priority stage: " << priority_stage << std::endl;        
            setupNextPriorityLevel();
            fill_constraints();
            fillHessian();

            sec_eval    = boost::chrono::high_resolution_clock::now() - start_eval;
            timing_eval += sec_eval.count();

            start_solve = boost::chrono::high_resolution_clock::now();


            /* convert to csc form */
            dense_to_csc();

            /* convert to C++ array form */
            A_x = (double*) a_x.data();
            A_i = (c_int*) a_i.data();
            A_p = (c_int*) a_p.data();
            H_x = (double*) h_x.data();
            H_i = (c_int*) h_i.data();
            H_p = (c_int*) h_p.data();
            l   = (double*) lbA.data();
            u   = (double*) ubA.data();
            q   = (double*) g.data();

            
            if (DEBUG) 
                std::cout << "*******  n: " << n << "    m: " << m << std::endl;        
            // Solve with OSQP
            data->n = n;
            data->m = m;
            data->P = csc_matrix(data->n, data->n, H_nnz, H_x, H_i, H_p);
            data->q = q;
            data->A = csc_matrix(data->m, data->n, A_nnz, A_x, A_i, A_p);
            data->l = l;
            data->u = u;
            // setup workspace
            if (settings) 
                osqp_set_default_settings(settings);

            settings->alpha = 1;
            if (DEBUG) 
                settings->verbose = 1;
            else
                settings->verbose = 0;
            // 	settings->verbose = 1;
            settings->max_iter = nWSR;
            // 	settings->warm_start = 0;
        
            retval = osqp_setup(&work, data, settings);

            /* fill in the primal solution of the previous iteration */
            // 	prev_sol_primal = Eigen::VectorXd::Zero(nv_robot+nv_feature+softcnr);
            prev_sol_primal.resize(nv_robot+nv_feature+softcnr); // resize the primal solution 
                prev_sol_primal.setZero();
            prev_sol_primal.head(nv_robot+nv_feature) = solution.head(nv_robot+nv_feature);
            for (int i=0; i< softcnr; ++i) 
                prev_sol_primal[nv_robot+nv_feature+i] = solution[nv_robot+nv_feature+prty_lvl_idx[i]];
            
            /* fill in the dual solution of the previous iteration */
            // 	prev_sol_dual.resize(
            if (DEBUG) {
                std::cout << "prev_sol_primal: [";
                for (int i = 0; i < prev_sol_primal.size() ; ++i) {
                    std::cout << prev_sol_primal[i] << ", ";
                }
                std::cout << "]" << std::endl;	    
            }
            
            // 	osqp_warm_start(work, (double*) prev_sol_primal.data(), (double*) prev_sol_dual.data());
            osqp_warm_start_x(work, (double*) prev_sol_primal.data());
            
            // solve the qp problem
            osqp_solve(work); 
            work->solution->x;
            
            if (work->info->status_val !=1) {
              std::cout << "solver status: " << work->info->status_val << std::endl;
	      if (work->info->status_val==-3)
		return retval;
              // 	  std::cout << "Printing the matrices ... " << std::endl;
              // 	  std::cout << "A: " << std::endl << A << std::endl;
              // 	  std::cout << "lbA: " << std::endl << lbA << std::endl;
              // 	  std::cout << "ubA: " << std::endl << ubA << std::endl;	  
            }
            
            if (DEBUG)	{
               std::cout << "size of solution: " << solution.size() << std::endl;
               std::cout << "priority level indices: ";
               for (int i=0; i < prty_lvl_idx.size(); ++i) {
                   std::cout << prty_lvl_idx[i] << ", ";
               }
               std::cout << "]" << std::endl; 
            }
                
            for (int i=0; i < softcnr; ++i) {
                if (DEBUG)  
                    std::cout << i << "  index: " << nv_robot+nv_feature+prty_lvl_idx[i] << std::endl;
                solution[nv_robot+nv_feature+prty_lvl_idx[i]] = work->solution->x[nv_robot+nv_feature+i];	
            }
                
            if (DEBUG)	{
                std::cout << "solution of the current priority level: [";
                for (int i=0; i < n; ++i) {
                    std::cout << work->solution->x[i] << ", ";
                }
                std::cout << "]" << std::endl;
                std::cout << "solution: [" << solution.transpose() << "]" << std::endl;
            }
 
            sec_eval     = boost::chrono::high_resolution_clock::now() - start_solve;
            timing_solve += sec_eval.count();
       
            // increment the priority level
            priority_stage++;	
        } // while
             
        for (int i=0; i< nv_robot+nv_feature; ++i) 
            solution[i] = work->solution->x[i];	// fill in the joint and feature variables velocities
        priority_stage = 2; // reset the priority stage for the next time step      
    } //if ... else 
  
         
      // Populate data
    //   if ((!initialization) && DEBUG) {
    //      std::cout << "Printing the data fed to the OSQP solver " << std::endl;
    //      std::cout << "\nH_nnz: " << H_nnz << std::endl;
    //      std::cout << "H_x: " << std::endl;
    //      for (int i=0 ; i< H_nnz; i++) 
    //      {
    //        std::cout << H_x[i] << " ";
    //      }
    //      std::cout << "\nH_i: " <<std::endl;
    //      for (int i=0 ; i< H_nnz; i++) 
    //      {
    //        std::cout << H_i[i] << " ";
    //      }
    //      std::cout << "\nH_p: "<<std::endl;
    //      for (int i=0 ; i< H.cols()+1; i++) 	
    //      {
    //        std::cout << H_p[i] << " ";
    //      }
    //      std::cout <<std::endl;
    //      std::cout << "\nA_nnz: " << A_nnz << std::endl;
    //      std::cout << "A_x: " << std::endl;
    //      for (int i=0 ; i< A_nnz; i++) 
    //      {
    //        std::cout << A_x[i] << " ";
    //      }
    //      std::cout << "size: " << a_x.size() << std::endl;
    //      std::cout << "\nA_i: " <<std::endl;
    //      for (int i=0 ; i< A_nnz; i++) 
    //      {
    //        std::cout << A_i[i] << " ";
    //      }
    //      std::cout << "\nA_p: "<<std::endl;
    //      for (int i=0 ; i< A.cols()+1; i++) 	
    //      {
    //        std::cout << A_p[i] << " ";
    //      }
    //      std::cout << "size: " << a_p.size() << std::endl;
    //      std::cout << "l: " << std::endl;
    //      for (int i=0 ; i< lbA.size(); i++) 
    //      {
    //        std::cout << l[i] << " ";
    //      }     
    //      std::cout <<std::endl;
    //      std::cout << "u: " << std::endl;
    //      for (int i=0 ; i< ubA.size(); i++) 
    //      {
    //        std::cout << u[i] << " ";
    //      }     
    //      std::cout <<std::endl;
    //      std::cout << "q: " << std::endl;
    //      for (int i=0 ; i< g.size(); i++) 
    //      {
    //        std::cout << q[i] << " ";
    //      }     
    //      std::cout <<std::endl;
    //      std::cout << "data->n: " << data->n << "  data->m: " << data->m << std::endl; 
    //      std::cout << "nc: " << nc << "  nv: " << nv << std::endl; 
    //   }  
        // Define Solver settings as default
      
       
      // Cleanup
    //   if (data) {
    //       if (data->A) free(data->A);
    //       if (data->P) free(data->P);
    //       free(data);
    //   }
    //   if (settings) free(settings);
      
    return retval;
} // hqpSolver::solve()

double hqpSolver::getWeightedResult() {
    //std::cout << solution.transpose() << std::endl;
    double val = 0.0;
    for (int i=optim_ndx.size();i<nv;++i) {
        //std::cout << i << " ===> " << solution[i] << "\t\t" << H(i,i) << std::endl;
        val +=  solution[i]*solution[i]*H(i,i);
    }
    return val;
}

int hqpSolver::updateStep(double dt) {
    int retval = solve();
    if (retval!=0) return retval;
    if (initialization) {
        // solution contains feature 
        // state    contains robot | feature | time  but (explicit) time remains constant.
        for (int i=0;i<nv_feature;++i) {
            state[nv_robot+i] += solution[i] * dt;
        } 
    } else {
        // solution contains robot | feature | slack velocities 
        // state    contains robot | feature | time
        for (int i=0;i<nv_robot+nv_feature;++i) {
            state[i] += solution[i] * dt;
        } 
        state[nv_robot+nv_feature] += dt;
    } 
    return 0;
}

/**
 * if you do not want to solve() but still want to evaluate all the 
 * expressions
 */
void hqpSolver::evaluate_expressions() { 
    expr_opt.setInputValues( state );
    solution.setZero(nv);   // fill zero in the solution vector
}


void hqpSolver::getJointNameToIndex(std::map<std::string,int>& namemap) {
    namemap.clear();
    for (int i=0;i<nv_robot;++i) {
        namemap[ ctx->getScalarStruct( all_ndx[i] )->name ] = i;
    }
}

void hqpSolver::getJointNameVector(std::vector<std::string>& namevec) {
    namevec.resize(nv_robot);
    for (int i=0;i<nv_robot;++i) {
        namevec[i] = ctx->getScalarStruct( all_ndx[i] )->name;
    }
}

void hqpSolver::getFeatureNameToIndex(std::map<std::string,int>& namemap) {
    namemap.clear();
    for (int i=0;i<nv_feature;++i) {
        namemap[ ctx->getScalarStruct( all_ndx[i+nv_robot] )->name ] = i;
    }
}

void hqpSolver::getFeatureNameVector(std::vector<std::string>& namevec) {
    namevec.resize(nv_feature);
    for (int i=0;i<nv_feature;++i) {
        namevec[i] = ctx->getScalarStruct( all_ndx[i+nv_robot] )->name;
    }
}

int hqpSolver::getNrOfJointStates() {
    return nv_robot;
}

int hqpSolver::getNrOfFeatureStates() {
    return nv_feature;
}

void hqpSolver::setJointStates(const Eigen::VectorXd& jntstate) {
    assert( jntstate.size() == nv_robot );
    for (int i=0;i<nv_robot;++i) {
        state[i] = jntstate[i];
    }
}

void hqpSolver::getJointStates(Eigen::VectorXd& jntstate) {
    assert( jntstate.size() == nv_robot );
    for (int i=0;i<nv_robot;++i) {
        jntstate[i] = state[i];
    }
}

void hqpSolver::setAndUpdateJointStates(const Eigen::VectorXd& jntstate) {
    assert( jntstate.size() == nv_robot );
    for (int i=0;i<nv_robot;++i) {
        state[i] = jntstate[i];
    }
    expr_opt.setInputValues( state );
}

void hqpSolver::setFeatureStates(const Eigen::VectorXd& featstate) {
    assert( featstate.size() == nv_feature );
    for (int i=0;i<nv_feature;++i) {
        state[i+nv_robot] = featstate[i];
    }
}

void hqpSolver::getFeatureStates(Eigen::VectorXd& featstate) {
    assert( featstate.size() == nv_feature );
    for (int i=0;i<nv_feature;++i) {
        featstate[i] = state[i+nv_robot];
    }
}

void hqpSolver::setTime( double time) {
    state[nv_robot+nv_feature] = time;
}

double hqpSolver::getTime(){
    return state[nv_robot+nv_feature];
}

void hqpSolver::getJointVelocities(Eigen::VectorXd& _jntvel){
    assert( !initialization );
    for (int i=0;i<nv_robot;++i) {
        _jntvel[i] = solution[i];
    }
}

void hqpSolver::getFeatureVelocities(Eigen::VectorXd& _featvel){
    assert( !initialization );
    for (int i=0;i<nv_feature;++i) {
        _featvel[i] = solution[i+nv_robot];
    }
}

double hqpSolver::getNormChange(){
    //assert( initialization );
    return sqrt(norm_change);
}

void hqpSolver::setState(const Eigen::VectorXd& _state) {  
    state = _state;
}

void hqpSolver::getState(Eigen::VectorXd& _state) {
    _state = state;
}


void hqpSolver::setInitialValues(){
     for (size_t i=0;i<all_ndx.size();++i) {
         VariableScalar* vs = ctx->getScalarStruct(all_ndx[i]);
         vs->initial_value = state[i];
     }
}

std::string hqpSolver::errorMessage(int code) {
    int nrOfMessages=0;
//     while (qpoases_messages[nrOfMessages]!=0) {
//         if (code == nrOfMessages) {
//             return qpoases_messages[code];
//         }
//         nrOfMessages++; 
//     }
    std::stringstream ss;
    ss << "UNKNOWN ERRORCODE " << code;
    return ss.str();
}

std::string hqpSolver::getName() {
    return "hqp_velocity_resolution";
}


}// end of namespace KDL

