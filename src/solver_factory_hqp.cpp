/**
 * file: solver_factory_hqp.cpp
 * purpose:
 *    implementation of SolverFactory_hqp: a factory class that can create an hqpSolver
 *    class with parameters given by a ParameterList data structure.
 * author:
 *    Erwin Aertbelien (c) 2019
 */
#include <expressiongraph/solver_factory_hqp.hpp>
#include <expressiongraph/solver_registry.hpp>
#include <expressiongraph/hqp_solver.hpp>

using namespace boost;
using namespace std;


namespace KDL {

    /**
     * SolverFactory_hqp : a solver factory that creates an instance of the qpoases solver
     */
    class SolverFactory_hqp : public SolverFactory {
            int    nWSR;
            double cputime;
            double regularization_factor;        
            bool   full_initialization;
            double weight_factor; 
            
        public:
            SolverFactory_hqp();
            virtual void getParameters( ParameterList& plist);

            virtual int  setParameters( const ParameterList& plist );
            virtual void getDocumentation( DocList& dlist );
            virtual boost::shared_ptr<solver> create();
            virtual SolverFactory::Ptr clone();
            virtual ~SolverFactory_hqp();
    };

    SolverFactory_hqp::SolverFactory_hqp() {
        nWSR = 1000;
        cputime = 1.0;
        regularization_factor = 1E-7;
        full_initialization = false;
        weight_factor       = 1000;
    }

    void SolverFactory_hqp::getParameters( ParameterList& plist) {
        plist["nWSR"]                  = nWSR;
        plist["cputime"]               = cputime;
        plist["regularization_factor"] = regularization_factor; 
        plist["full_initialization"]   = full_initialization;
        plist["weight_factor"]         = weight_factor;
    }

    void SolverFactory_hqp::getDocumentation( DocList& dlist ) {
        dlist["nWSR"] = "The maximum number of hqp iterations during one sample period";
        dlist["cputime"] = "(Approximately) the maximum cpu time to use for one sample period";
        dlist["regularization_factor"] = "The squared norm of the robot and feature velocities is multiplied by the regularisation_factor and added to the optimisation criterion";
        dlist["weight_factor"] = "During initialisation the hard constraints are translated into soft constraints, multiplied by weight_factor";
        dlist["full_initialization"] = "If true, a full initialization procedure is used, if false, the old initialization is used";
    }


    int  SolverFactory_hqp::setParameters( const ParameterList& plist ) {
        int result =0;
        for (ParameterList::const_iterator it = plist.begin();it!=plist.end();++it) {
            if (it->first.compare("nWSR")==0) {
                nWSR = (int)it->second;
            } else if (it->first.compare("cputime")==0) {
                cputime = it->second;
            } else if (it->first.compare("regularization_factor")==0) {
                regularization_factor = it->second;
            } else if (it->first.compare("weight_factor")==0) {
                weight_factor = it->second;
            } else if (it->first.compare("full_initialization")==0) {
                full_initialization =  ( fabs(it->second - 1.0) < 0.001);
            } else {
                result =  -1;
            }
        }
        return result;
    }


    solver::Ptr SolverFactory_hqp::create() {
        return boost::make_shared< hqpSolver>(nWSR, cputime, regularization_factor);
    }


    SolverFactory_hqp::~SolverFactory_hqp() {}

    SolverFactory::Ptr SolverFactory_hqp::clone() {
        // using the automatically generated copy constructor:
        return  make_shared<SolverFactory_hqp>(*this); 
    }


    void registerSolverFactory_hqp( SolverRegistry::Ptr reg, const std::string& name ) {
        boost::shared_ptr<SolverFactory_hqp> p = boost::make_shared<SolverFactory_hqp>();
        reg->registerFactory(p,name);
    }

}; // namespace KDL;

