import csv
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.animation as animation
from numpy import sin, cos

data_file = open('data.txt')
tsv_reader = csv.reader(data_file, delimiter='\t',
                        quoting=csv.QUOTE_NONNUMERIC)

class PlanarRobot:
    """Planar Robot Class
    """
    def __init__(self,
                 init_state = [0, 0, 0],
                 L1=0.2,
                 L2=0.2, 
                 L3=0.2, 
                 origin=(0, 0)): 
        self.init_state = np.asarray(init_state, dtype='float')
        self.params = (L1, L2, L3)
        self.origin = origin
        self.time_elapsed = 0

        self.state = self.init_state
    
    def position(self):
        """compute the current x,y positions of the pendulum arms"""
        (L1, L2, L3) = self.params

        x = np.cumsum([self.origin[0],
                       L1 * cos(self.state[0]),
                       L2 * cos(self.state[0]+self.state[1]),
                       L3 * cos(self.state[0]+self.state[1]+self.state[2])])
        y = np.cumsum([self.origin[0],
                       L1 * sin(self.state[0]),
                       L2 * sin(self.state[0]+self.state[1]),
                       L3 * sin(self.state[0]+self.state[1]+self.state[2])])
        #x = np.cumsum([self.origin[0],
                       #L1 * cos(self.state[0]),
                       #L2 * cos(self.state[0]+self.state[1])])
        #y = np.cumsum([self.origin[0],
                       #L1 * sin(self.state[0]),
                       #L2 * sin(self.state[0]+self.state[1])])
        return (x, y)
    
    def updateState(self, x):
        """update the joint state"""
        self.state = x

ncols = len(next(tsv_reader))
Data1 = []
Data2 = []
Data3 = []
Data4 = []
Data5 = []
Data6 = []
Data7 = []
rowcount = 0
for row in tsv_reader:
   Data1.append(row[0])
   Data2.append(row[1])
   Data3.append(row[2])
   Data4.append(row[3])
   Data5.append(row[4])
   Data6.append(row[5])
   Data7.append(row[6])
   rowcount = rowcount+1
   #for col in range(0, ncols):
    #print(row[col])
    #print(Data[0])
#    Data[] = row[col]
    #print(Data[rowcount][col])
    
Ndata = rowcount

#plt.plot(Data4)
#plt.show()

#plt.plot(Data5)
#plt.show()

#plt.plot(Data6)
#plt.show()

#plt.plot(Data7)
#plt.show()


#------------------------------------------------------------
# set up initial state and global variables
robot = PlanarRobot([0.0, 0.01, -0.02])
dt = 1./30 # 30 fps

#------------------------------------------------------------
# set up figure and animation
fig = plt.figure()
ax = fig.add_subplot(111, aspect='equal', autoscale_on=False,
                     xlim=(-0.6, 0.6), ylim=(-0.6, 0.6))
ax.grid()

line, = ax.plot([], [], 'o-', lw=2)

def init():
    """initialize animation"""
    line.set_data([], [])
    return line,

def animate(i):
    """perform animation step"""
    global robot
    x = [Data1[i], Data2[i], Data3[i]]
    robot.updateState(x)
    line.set_data(*robot.position())
    return line,

# choose the interval based on dt and the time to animate one step
from time import time
t0 = time()
animate(0)
t1 = time()
interval = 1000 * dt - (t1 - t0)
ani = animation.FuncAnimation(fig, animate, frames=Ndata,
                              interval=interval, blit=True, init_func=init)

# save the animation as an mp4.  This requires ffmpeg or mencoder to be
# installed.  The extra_args ensure that the x264 codec is used, so that
# the video can be embedded in html5.  You may need to adjust this for
# your system: for more information, see
# http://matplotlib.sourceforge.net/api/animation_api.html
#ani.save('double_pendulum.mp4', fps=30, extra_args=['-vcodec', 'libx264'])

plt.show()
