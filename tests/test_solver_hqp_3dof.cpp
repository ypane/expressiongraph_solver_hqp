#include <expressiongraph/context.hpp>
#include <expressiongraph/hqp_solver.hpp>
#include <expressiongraph/outputs_matlab.hpp>
#include <expressiongraph/controller.hpp>
#include <kdl/expressiontree.hpp>
#include <chrono>

using namespace KDL;
using namespace Eigen;
using namespace std;

void update_and_check( hqpSolver& s, double dt ) {
    int c=s.updateStep(dt);
    //s.printMatrices(cout);
    if (c!=0) {
        cerr << "solved encountered error : \n";
        cerr << s.errorMessage(c)  << endl;
    }
}

int main(int argc, char* argv[]) {
    cout << "creating context..." << endl;
    Context::Ptr ctx = create_context();

    cout << "adding variable types..." << endl;
    // set-up necessary for robot control problems:
    ctx->addType("robot");
    ctx->addType("feature");
    ctx->addType("time");

    cout << "defining example problem..." << endl;
    // define the problem: a 3-dof robot in 2d-space
    // not the best way, just an illustration
    Expression<double>::Ptr q1    = ctx->addScalarVariable("q1","robot", 0.0, Constant(1.0));
    Expression<double>::Ptr q2    = ctx->addScalarVariable("q2","robot", 0.01, Constant(1.0));
    Expression<double>::Ptr q3    = ctx->addScalarVariable("q3","robot", -0.02, Constant(1.0));
    Expression<double>::Ptr L1    = Constant(0.2);
    Expression<double>::Ptr L2    = Constant(0.2);
    Expression<double>::Ptr L3    = Constant(0.2);
    Expression<double>::Ptr shoulder_x  = L1*cos(q1);
    Expression<double>::Ptr shoulder_y  = L1*sin(q1);
    Expression<double>::Ptr elbow_x  = shoulder_x+L2*cos(q1+q2);
    Expression<double>::Ptr elbow_y  = shoulder_y+L2*sin(q1+q2);
    Expression<double>::Ptr wrist_x  = elbow_x+L3*cos(q1+q2+q3);
    Expression<double>::Ptr wrist_y  = elbow_y+L3*sin(q1+q2+q3);
    Expression<double>::Ptr des_x    = Constant(0.1);
    Expression<double>::Ptr des_y    = Constant(0.4);

    ControllerRegistry reg;
    reg.register_controller(create_controller_proportional());
    reg.register_controller(create_controller_proportional_saturated());
    Controller::Ptr c = reg.lookupPrototype("proportional");
    c->setParameter("K", Constant<double>(1.0));
    ctx->addInequalityConstraint("tracking_x", 
            wrist_x - des_x,    wrist_x - des_x, 
            0.0, 0.0, c->clone(), c->clone(), 
            Constant<double>(1.0), 4);
    ctx->addInequalityConstraint("tracking_y", 
            wrist_y - des_y,    wrist_y - des_y, 
            0.0, 0.0, c->clone(), c->clone(), 
            Constant<double>(1.0), 4);
    ctx->addInequalityConstraint("maintain_shoulder_height", 
            shoulder_y,    shoulder_y, 
            0.1, 0.1, c->clone(), c->clone(), 
            Constant<double>(1.0), 2);
    ctx->addInequalityConstraint("elbow_x", 
            elbow_x,    elbow_x, 
            0.1, 0.1, c->clone(), c->clone(), 
            Constant<double>(1.0), 3);
    ctx->addInequalityConstraint("elbow_y", 
            elbow_y,    elbow_y, 
            0.0, 0.18, c->clone(), c->clone(), 
            Constant<double>(1.0), 3);
    ctx->addBoxConstraint("q1d_max", 1, -5, 5);
    ctx->addBoxConstraint("q2d_max", 2, -5, 5);
    ctx->addBoxConstraint("q3d_max", 3, -5, 5);
    
    // Print constraints so to know how they are ordered
    for (uint indx=0; indx<ctx->cnstr_scalar.size(); ++ indx) {
      ConstraintScalar& c = ctx->cnstr_scalar[indx];
      std::cout << "Constraint " << indx << ": " <<  c.name << ", priority: " << c.priority << std::endl;
    }
    
    // specification of the output:
    ctx->addOutput<double>("q1","matlab",q1); 
    ctx->addOutput<double>("q2","matlab",q2); 
    ctx->addOutput<double>("q3","matlab",q3); 
//     ctx->addOutput<double>("dx","matlab",wrist_x); 
//     ctx->addOutput<double>("dy","matlab",wrist_y);
    ctx->addOutput<double>("dx","matlab",wrist_x - des_x); 
    ctx->addOutput<double>("dy","matlab",wrist_y - des_y); 
    ctx->addOutput<double>("shoulder_y","matlab",shoulder_y); 
    ctx->addOutput<double>("elbow_y","matlab",elbow_y); 

    OutputGenerator::Ptr outgen =  create_matlab_output(cout,"matlab");

    cout << ctx << endl;
    
    hqpSolver solver(100,0.0, 1E-5);
    std::cout << "preparing initialization ... " << std::endl;
    solver.prepareInitialization(ctx);
    for (int i=0;i<100;++i) {
        solver.updateStep(0.01);
    }
    std::cout << "preparing execution ... " << std::endl;
    solver.prepareExecution(ctx);
    outgen->init(ctx);
    std::cout << "solving execution ... " << std::endl;
    outgen->update(ctx);
    Eigen::VectorXd outp;
    auto start = chrono::steady_clock::now();
    for (int i=0;i<1000;++i) {
// 	cout << ctx << endl;
        update_and_check(solver,0.01);
        outgen->update(ctx);
// 	ctx->outputToVector("", outp);
    }
    auto end = chrono::steady_clock::now();
    cout << "Elapsed time in microseconds : " 
         << chrono::duration_cast<chrono::microseconds>(end - start).count()
         << " µs" << endl;

    outgen->finish(ctx);
    return 0;
}

