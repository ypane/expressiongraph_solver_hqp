#ifndef EXPRESSIONGRAPH_SOLVER_FACTORY_HQP
#define EXPRESSIONGRAPH_SOLVER_FACTORY_HQP

/**
 * file: expressiongraph_solver_factory_hqp.hpp
 *
 * purpose:
 *   defines a solver factory that creates an instance of the qpoases solver for eTaSL
 *
 * author: 
 *   Erwin Aertbelien, 2019
 *
 * part of eTaSL
 */
#include <expressiongraph/solver_registry.hpp>


namespace KDL {
    /**
     * registers a factory for a hqpSolver to the given registry
     * \param reg: registry
     * \param name: name under which to register it
     */ 
    void registerSolverFactory_hqp(SolverRegistry::Ptr reg, const std::string& name );

}; // namespace KDL

#endif
