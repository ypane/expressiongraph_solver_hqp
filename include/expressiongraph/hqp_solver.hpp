#ifndef EXPRESSIONGRAPH_TF_HQP_SOLVER_HPP
#define EXPRESSIONGRAPH_TF_HQP_SOLVER_HPP

#include <expressiongraph/context.hpp>
#include <osqp.h>
#include <Eigen/Dense>
#include <kdl/conversions.hpp>
#include <expressiongraph/solver.hpp>

namespace KDL {

#define HUGE_VALUE 1e20
  
class hqpSolver: public solver {
protected:
  // reset the problem before filling it in:
  void reset();
  
  int setupCommon(Context::Ptr _ctx);
  
  void setupMatrices(int nv, int nc);
  
  void fillHessian();
  
  void fillConstraintScalar(const ConstraintScalar& c);

  void fillConstraintBox(const ConstraintBox& c);

  void fill_constraints();

  std::vector<int>    nc_priority;        ///< number of constraints for each priority number:
  std::vector<int>    prty_lvl_idx;       ///< indices of the soft constraints at a given priority level
  int                 nc;                 ///< total number of constraints
  int                 nc_soft;            ///< total number of soft constraints i.e. priority >= 2
  Eigen::Matrix<double,Eigen::Dynamic,Eigen::Dynamic,Eigen::RowMajor> A;    ///< constraint matrix  A
  Eigen::VectorXd     lb;                 ///< lb <= x
  Eigen::VectorXd     ub;                 ///< x <= ub
  Eigen::VectorXd     lbA;                ///< lba <= A*x
  Eigen::VectorXd     ubA;                ///< A*x <= ubA
  Eigen::Matrix<double,Eigen::Dynamic,Eigen::Dynamic,Eigen::RowMajor> H;    ///< optimize x'*H*x + g'*x
  Eigen::VectorXd     g;                  ///< optimize x'*H*x + g'*x
  Eigen::VectorXd     prev_sol_primal;
//   Eigen::VectorXd prev_sol_dual;  
//   Eigen::MatrixXd DD; 
        
  // OSQP variables
  OSQPWorkspace *work;
  OSQPData *data;
  OSQPSettings *settings;
  
  // CSC sparse matrix variables in std::vector 
  std::vector<double> h_x;
  std::vector<c_int> h_i;
  std::vector<c_int> h_p;
  std::vector<double> a_x;
  std::vector<c_int> a_i;
  std::vector<c_int> a_p;

  // variable to store the solution of the different stages of HQP 
//   Eigen::VectorXd     solution_i;           ///< results from the optimalisation: joint and feature velocities, slack variables
  
  // CSC sparse matrix variables in C++ array
  double* H_x; // the non-zero elements of the hessian matrix
  c_int* H_i; 
  c_int* H_p;
  double* A_x; // the non-zero elements of the constraint matrix
  c_int* A_i;
  c_int* A_p;  
  double* q;

  // other optimization-related vectors/scalars
  c_int H_nnz; // number of non-zero elements in the hessian matrix
  c_int A_nnz; // number of non-zero elements in the constraint matrix
  double* g_x;
  double* l; // the constraints' lower bound array
  double* u; // the constraints' upper bound array
  c_int n; // number of rows of hessian matrix
  c_int m; // number of rows of constraint matrix
  
  int                 softcnr;            ///<  counter to fill in the soft constraints.
  int                 cnr;                ///<  counter to fill in (all) constraints relevant for one priority stage
  int                 cnr_global;         ///<  counter to fill in (all) constraints

  double              regularisation_factor;   ///< regularisation factor double regularisation_factor;
  int                 nWSR;               ///< maximum number of solver iterations. 
  double              cputime;            ///< maximum execution time.
  
  int                 priority_stage; /// < index to solve a specific priority level, starts from 2 
  
  void setupNextPriorityLevel(); /// < solve the QP problem of the next priority level

  double              timing_eval;        ///< timing of the evaluation of the expressions
  double              timing_solve;       ///< timing of the solving of the QP problem 


public:

    /**
     * \param [in]  nWSR  maximum number of solver iterations.
     * \param [in]  cputime maximum execution time. If this value is equal to zero, it is ignored.
     * \param [in]  regularisation_factor   regularisation_factor to be used.
     */
    hqpSolver(int nWSR,double cputime, double regularisation_factor);

    /**
     * get the timing of the evaluation of the expressions
     * (in seconds, floating point)
     * Default implementation for compatibility with previous release
     * Should be overridden by child classes.
     */
    virtual double getTimingEval() {
        return timing_eval;
    }
    
    /**
     * get the timing of the evaluation of the expressions
     * (in seconds, floating point)
     * Default implementation for compatibility with previous release
     * Should be overridden by child classes.
     */
    virtual double getTimingSolve() {
        return timing_solve;
    }

    /// set the qpOASES print level
    void setPrintLevel(int n);
    
  /**
    * Prepare for solving during an iterative initialization.
    * \param [in] ctx Context to initialize  the solver for.
    * \param [in] time time to start execution with.
    * \return 0 if sucessful, returns:
    *    - -1 if an unknown priority level is used.
    */
  virtual int prepareInitialization(Context::Ptr ctx);

  /**
    * set the initial values in the context to the current state.
    * (typically done after an initialization run).
    */    


  void printMatrices(std::ostream& os);
  
   
  /**
   * convert a dense matrix format to
   * a sparse (CSC) format 
   */
  void dense_to_csc();

  /**
    * Prepare for solving during an iterative execution.
    * \param [in] ctx Context to initialize  the solver for.
    * \param [in] time time to start execution with.
    * \return 0 if sucessful, returns:
    *    - -1 if an unknown priority level is used.
    */
  virtual int prepareExecution(Context::Ptr ctx);


  /**
    * solves the optimization problem.
    * The typical call pattern is: 
    \code{.cpp} 
    \endcode 
    * \return 0 if successfull, use error_message method to interprete the error codes.
    */
  virtual int solve();

  virtual double getWeightedResult();

  
  virtual int  updateStep(double dt);
    
  /**
    * returns a description of the error codes returned by solve().
    */
  virtual  std::string errorMessage(int code) ;

  virtual std::string getName();

  virtual void evaluate_expressions();

  virtual void getJointNameToIndex(std::map<std::string,int>& namemap);
  virtual void getJointNameVector(std::vector<std::string>& namevec);
  virtual void getFeatureNameToIndex(std::map<std::string,int>& namemap);
  virtual void getFeatureNameVector(std::vector<std::string>& namevec);
  virtual int  getNrOfJointStates();
  virtual int  getNrOfFeatureStates();
 
  /**
    * /caveat jntstate should have the correct size ( nr of joints).
    */
  virtual void setJointStates(const Eigen::VectorXd& jntstate);
  virtual void getJointStates(Eigen::VectorXd& jntstate);

  /**
    * /caveat jntstate should have the correct size ( nr of joints).
    * works as setJointStates, but also updates the relations on which constraints are expressed.
    * after this call is possible to ask for jacobians from expressions
    */
  virtual void setAndUpdateJointStates(const Eigen::VectorXd& jntstate);

  /**
    * /caveat featstate should have the correct size ( nr of feature variables).
    */
  virtual void setFeatureStates(const Eigen::VectorXd& featstate);
  virtual void getFeatureStates(Eigen::VectorXd& featstate);
  virtual void setTime( double time);
  virtual double getTime();

  /**
    * /caveat not to be called during the initialization phase.
    */
  virtual void getJointVelocities(Eigen::VectorXd& _jntvel);

  /**
    * /caveat not to be called during the initialization phase.
    */
  virtual void getFeatureVelocities(Eigen::VectorXd& _featvel);


  /**
    * returns the norm af the relevant state, ie feature variables for an initialisation step,
    * robot + feature variables for execution step.
    * only valid to call during initialization.
    */
  virtual double getNormChange();
      
  /**
    * Sets the state variable (robot | feature | time) to the given values.
    */
  virtual void setState(const Eigen::VectorXd& _state);

  /**
    * Gets the state variable (robot | feature | time).
    */
  virtual void getState(Eigen::VectorXd& _state); 


  /**
    * set the initial values in the context to the current state.
    * (typically done after an initialization run).
    */    
  virtual void setInitialValues();

  virtual ~hqpSolver() {}
};

} // namespace KDL
#endif
